from flask import Flask, request, make_response
import cloudscraper

app = Flask(__name__)


@app.route("/", methods=['GET'])
def search():
    args = request.args
    url = args.get('url', default="https://tap.az")
    token = args.get('token')

    if token != "64e46ae4aad99ec873762c70314dae91":
        return make_response({
            'success': False,
            'message': 'Token is invalid'
        }, 400)
    else:
        scraper = cloudscraper.create_scraper(browser={
            'browser': 'chrome',
            'platform': 'windows',
            'mobile': False
        })

        return scraper.get(url).text


if __name__ == "__main__":
    app.run()
